package c3127333;
/**
 * COMP2230/6230 Introduction to Algorithmics
 *               Assignment
 *                  2012
 * 
 * Run Kruskals and Prims algorithms on a set of graphs
 * to create Minimum Spanning Trees (MST)
 * 
 * I have the ability to generate some test data but i have used it once 
 * and commented it out
 * 
 * The main will take graph input from the standard input.
 * 
 * 
 * @author Ross Bille 3127333 
 * @version 1
 */
import c3127333.algorithms.Edge;
import c3127333.algorithms.Graph;
import c3127333.algorithms.MinHeap;
import c3127333.algorithms.Timer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;
public class RoadRebuilding
{
    private static String endl;
    public static void main (String[] args)
    {
        endl =  String.format("%n");
        //used for generating complete graphs and writing to file
        //ArrayList<Graph> list = new ArrayList<Graph>();
        //list.add(generateComplete(5, "k5"));
        //list.add(generateComplete(10, "k10"));
        //list.add(generateComplete(15, "k15"));
        //list.add(generateComplete(40, "k40"));
        //list.add(generateComplete(100, "k100"));
        //write("complete_Graphs", list);
        
        
        ArrayList<Graph> graphs;
        graphs = input();
        
        LinkedList<Result> results = new LinkedList<Result>();
        int count = 0;
        System.out.println("running Kruskal's");
        for(Graph g : graphs)
            results.add(kruskal(g,++count));
        count=0;
        System.out.println("running Prim's");
        for(Graph g:graphs)
            results.add(prim(g,++count));
        System.out.println("printing results");
        write("results",results);
        System.out.println("Complete");

        //write("results",graphs);
        //for(Result r : results)
        //    System.out.println(r);
    }
    private static ArrayList<Graph> input()
    {
        ArrayList<Graph> graphs = new ArrayList<Graph>();
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        while(!str.equals("EOF"))
        {
            //make new graph with value of str as name and the next integer as the size
            int tempSize = in.nextInt();
            //read the edges
            ArrayList<Edge> tempEdges = new ArrayList<Edge>();
            while(in.hasNextInt())
                tempEdges.add(new Edge(in.nextInt(), in.nextInt(), in.nextInt()));
            Graph tempGraph = new Graph(str);
            tempGraph.setEdges(tempEdges);
            ArrayList<Integer> tempVert = new ArrayList<Integer>();
            for(int i=1;i<tempSize+1;i++)
                tempVert.add(i);
            tempGraph.setVert(tempVert);
            
            graphs.add(tempGraph);
            
            in.nextLine();
            str = in.nextLine();
            //System.out.println(tempGraph);
        }
        return graphs;
        
    }
    private static void write(String name, LinkedList<Result> results)
    {
        try
        {
            FileWriter fw = new FileWriter(name+".txt");
            BufferedWriter out = new BufferedWriter(fw);
            for(Result r : results)
                out.write(r.toString());
            out.write("EOF");
            out.close();
        }catch (Exception e)
        {
            System.err.println("Error: " + e.getMessage());
        }
    }
    private static void write(String name,ArrayList<Graph> graphs)
    {
        try
        {
            FileWriter fw = new FileWriter(name+".txt");
            BufferedWriter out = new BufferedWriter(fw);
            for(Graph g:graphs)
                out.write(g.toString());
            out.write("EOF");
            out.close();
        }catch (Exception e)
        {
            System.err.println("Error: " + e.getMessage());
        }
    }
    private static Result prim(Graph g, int count)
    {
        Timer t = new Timer();
        boolean[] nonTree = new boolean[g.getVertices().size()];
        Arrays.fill(nonTree,true);
        Graph prims = new Graph("Prims");
        MinHeap<Edge> heap = new MinHeap<Edge>();
        //choose start vertex
        Integer start = g.getVertices().get(0);
        prims.addVertex(start);
        nonTree[0]=false;
        //start filling the heap
        addToHeap(heap, g.getEdges(), start);        
        while(prims.getVertices().size()<g.getVertices().size())
        {
            Edge tempEdge = heap.getMin();
            ArrayList<Integer> vertices = prims.getVertices();
            //if edge connects a tree and non tree component
            //  add the edge and corresponding vertex to the tree
            //  add adjacent edges to the heap
            if(vertices.contains(tempEdge.getFirst())&&nonTree[tempEdge.getLast()-1])
            {
                //v1 is in the tree and v2 is not
                nonTree[tempEdge.getLast()-1]=false;
                prims.addVertex(tempEdge.getLast());
                prims.addEdge(tempEdge);
                heap.remove();
                addToHeap(heap, g.getEdges(), tempEdge.getLast());                
            }else if(vertices.contains(tempEdge.getLast())&&nonTree[tempEdge.getFirst()-1])
            {
                //v2 is in the tree and v1 is not
                nonTree[tempEdge.getFirst()-1]=false;
                prims.addVertex(tempEdge.getFirst());
                prims.addEdge(tempEdge);
                heap.remove();
                addToHeap(heap, g.getEdges(), tempEdge.getFirst());
            }else{
                //edge would make a loop, remove from top of heap
                heap.remove();
            }
        }
        long runtime = t.getCurrentRunTime();
        //store results
        Result result = new Result(runtime, prims, "Prim "+count, g.getName());
        //return results
        return result;
    }
    //method for adding all of vertex i's adjacent edges to the heap
    private static void addToHeap(MinHeap<Edge> mh, ArrayList<Edge> al, Integer i)
    {
        for(Edge e:al)
            if(e.getFirst()==i||e.getLast()==i)
                mh.add(e);        
    }
    private static Result kruskal(Graph g, int count)
    {
        Timer t = new Timer();
        //set up forrest
        ArrayList<Graph> forrest = new ArrayList<Graph>();
        
        for(Integer v:g.getVertices())
        {
            Graph tempGraph = new Graph("Kruskals");
            tempGraph.addVertex(v);
            tempGraph.setEdges(new ArrayList<Edge>());
            forrest.add(tempGraph);
        }
        //sort edges
        ArrayList<Edge> sortedEdges = g.getEdges();
        Collections.sort(sortedEdges);
        for(Edge e: sortedEdges)
        {
            //check if spanning tree
            if(forrest.size()==1)
                break;
            //if e connects 2 different trees, merge the 2 trees with that edge
            Graph component_a = whichComponent(forrest, e.getFirst());
            Graph component_b = whichComponent(forrest, e.getLast());
            if(!sameComponent(component_a,component_b))
            {
                forrest.remove(component_a);
                forrest.remove(component_b);
                forrest.add(merge(component_a,component_b,e,"kruskals"));
            }
        }
        long runtime = t.getCurrentRunTime();
        Result result = new Result(runtime, forrest.get(0), "Kruskal"+count, g.getName());
        return result;
    }
    //method for deciding which graph the vertex v is in
    private static Graph whichComponent(ArrayList<Graph> components, Integer v)
    {
        for(Graph g:components)
            if(g.getVertices().contains(v))
                return g;
        return null;
    }
    //check to see if the 2 graphs are the same
    private static boolean sameComponent(Graph a, Graph b)
    {
        if(a==b)
            return true;
        return false;
    }
    //merge 2 graphs
    private static Graph merge(Graph g1, Graph g2, Edge e, String alg)
    {
        Graph finalGraph = new Graph(alg);
        //add all edges from both graphs to the new graph
        for(Edge e1: g1.getEdges())
            finalGraph.addEdge(e1);
        for(Edge e2: g2.getEdges())
            finalGraph.addEdge(e2);
        //add the extra edge
        finalGraph.addEdge(e);
        //add all vertices from both graphs to the new graph
        finalGraph.setVert(g1.getVertices());
        finalGraph.getVertices().addAll(g2.getVertices());
        return finalGraph;
    }
    //used to generate a complete graph with n vertices  Kn
    private static Graph generateComplete(int n, String name)
    {
        Graph graph = new Graph(name);
        for(int i=1; i<=n; i++)
        {
            graph.addVertex(i);
            for(int j=i;j<=n;j++)
            {
                if(j!=i)
                {
                    int weight = 1 +(int)(Math.random()*100);
                    Edge edge = new Edge(i, j, weight);
                    graph.addEdge(edge);
                }
            }
        }
        return graph;
    }
    //result object to store all the results of both the algorithms
    private static class Result
    {
        private long runtime;
        private Graph mst;
        private int totalWeight;
        private String algorithmName, graphName;

        public Result(long runtime, Graph mst, String algorithmName, String graphName) {
            this.runtime = runtime;
            this.mst = mst;
            this.algorithmName = algorithmName;
            this.graphName = graphName;
            int weight = 0;
            for(Edge e:mst.getEdges())
                weight += e.getWeight();
            totalWeight = weight;
            
        }
        public Result() 
        {
            this(0, new Graph(), "none", "none");
        }

        public String getAlgorithmName() {
            return algorithmName;
        }

        public void setAlgorithmName(String algorithmName) {
            this.algorithmName = algorithmName;
        }

        public String getGraphName() {
            return graphName;
        }

        public void setGraphName(String graphName) {
            this.graphName = graphName;
        }

        public Graph getMst() {
            return mst;
        }

        public void setMst(Graph mst) {
            this.mst = mst;
        }

        public long getRuntime() {
            return runtime;
        }

        public void setRuntime(long runtime) {
            this.runtime = runtime;
        }

        public int getTotalWeight() {
            return totalWeight;
        }

        public void setTotalWeight(int totalWeight) {
            this.totalWeight = totalWeight;
        }

        @Override
        public String toString() 
        {
            String str = algorithmName + endl + runtime + "ms"+endl + totalWeight + endl + mst.edgesToString();
            return str;
        }
    }
}