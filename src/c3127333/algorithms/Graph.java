/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package c3127333.algorithms;

import java.util.ArrayList;
/**
 *
 * @author Ross Bille 3127333
 */
public class Graph implements Cloneable
{
    private static String endl = String.format("%n");
    private String name;
    private ArrayList<Edge> edges;
    private ArrayList<Integer> vertices;
    public Graph(String n)
    {
        name = n;
        edges = new ArrayList<Edge>();               
        vertices = new ArrayList<Integer>();
      
    }
    public Graph()
    {
        name = "noname";
        edges = new ArrayList<Edge>();               
        vertices = new ArrayList<Integer>();
      
    }
    public void addEdge(Edge e){edges.add(e);}
    public void addVertex(Integer i){vertices.add(i);}
    public ArrayList<Edge> getEdges(){return edges;}
    public void setEdges(ArrayList<Edge> e){edges = e;}
    public void setVert(ArrayList<Integer> v){vertices = v;}
    public ArrayList<Integer> getVertices(){return vertices;}
    @Override
    public String toString()
    {
        String str = name+endl+vertices.size()+endl;
        for(Edge e : edges)
            str += e + endl;
        return str;
    }
    public String edgesToString()
    {
        String str = "";
        for(Edge e : edges)
            str += e + endl;
        return str;
    }
    public String getName() {
        return name;
    }
    @Override
    public Graph clone() throws CloneNotSupportedException
    {
        return (Graph) super.clone();
    }
}