package c3127333.algorithms;

/**
 * Used to time algorithm execution times in milliseconds
 * @author Ross Bille 3127333
 */
public class Timer {

    long startTime;
    public Timer() 
    {
        startTime = System.currentTimeMillis();
    }
    public long getCurrentRunTime()
    {
        long currentTime = System.currentTimeMillis();
        return currentTime-startTime;
    }
}
