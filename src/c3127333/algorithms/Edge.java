package c3127333.algorithms;

/**
 * Edge class used to store edges as 2 vertices and a weight
 * @author Ross Bille 3127333
 */
public class Edge implements Comparable<Edge>, Cloneable
    {
    private int v1, v2;
    private int weight;
    public Edge()
    {
        v1 = 0;
        v2 = 0;
        weight = 0;
    }
    public Edge(int x, int y, int w)
    {
        v1 = x;
        v2 = y;
        weight = w;
    }
    public int getWeight(){return weight;}
    public void setWeight(int w){weight = w;}
    public int getFirst(){return v1;}
    public int getLast(){return v2;}
    public void setFirst(int v){v1=v;}
    public void setLast(int v){v2=v;}
    @Override
    public String toString()
    {
        return String.format("%d %d %d",v1,v2,weight);
    }
    @Override
    public int compareTo(Edge t) 
    {
        return this.getWeight()-t.getWeight();
    }
    public Edge copy() throws CloneNotSupportedException
    {
        return (Edge)super.clone();
    }
}