package c3127333.algorithms;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Generic minHeap class
 * This is used for storing adjacent edges during Prims Algorithm
 * @author Ross
 */
public class MinHeap<E extends Comparable<E>>
{
    ArrayList<E> heap;
    public MinHeap()
    {
        heap = new ArrayList<E>();
    }
    public MinHeap(E[] keys)
    {
        heap.addAll(Arrays.asList(keys));
        for(int i=heap.size()/2-1;i>=0;i--)
            siftDown(i, heap.get(i));
    }
    public void add(E obj)
    {
        heap.add(null);
        int i = heap.size()-1;
        while(i>0)
        {
            int parentIndex = (i-1)/2;
            E parent = heap.get(parentIndex);
            if(obj.compareTo(parent)>=0)
                break;
            heap.set(i,parent);
            i = parentIndex;
        }
        heap.set(i, obj);
    }
    public E remove()
    {
        E obj = heap.get(0);
        E last = heap.remove(heap.size()-1);
        siftDown(0, last);
        return obj;
    }
    private void siftDown(int i, E obj) 
    {
        if(!heap.isEmpty())
        {
            while(i<heap.size()/2)
            {
                int child = 2*i+1;
                if(child<heap.size()-1 && heap.get(child).compareTo(heap.get(child+1))>0)
                    child++;
                if(obj.compareTo(heap.get(child))<=0)
                    break;
                heap.set(i, heap.get(child));
                i=child;
            }
            heap.set(i,obj);
        }
    }
    public E getMin(){return heap.get(0);}
}
